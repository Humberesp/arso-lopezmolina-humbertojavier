package modelo;

public class TareaPendiente {
	private String id;
	private String titulo;
	private String organizador;
	private String idTarea;
	private String fechaLimite;
	private String tipo;
	private String correoAlumno;	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getOrganizador() {
		return organizador;
	}
	public void setOrganizador(String organizador) {
		this.organizador = organizador;
	}
	public String getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(String idTarea) {
		this.idTarea = idTarea;
	}
	public String getFechaLimite() {
		return fechaLimite;
	}
	public void setFechaLimite(String fechaLimite) {
		this.fechaLimite = fechaLimite;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCorreoAlumno() {
		return correoAlumno;
	}
	public void setCorreoAlumno(String correoAlumno) {
		this.correoAlumno = correoAlumno;
	}
	
	
}
