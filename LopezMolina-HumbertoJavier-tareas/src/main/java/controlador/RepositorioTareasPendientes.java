package controlador;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

import modelo.TareaPendiente;

public class RepositorioTareasPendientes {
	private final MongoCollection<Document> tareasPendientes;

	public RepositorioTareasPendientes(MongoCollection<Document> tareasPendientes) {
		this.tareasPendientes = tareasPendientes;
	}

	private TareaPendiente tareaPendiente(Document doc) {
		TareaPendiente tareaPendiente = new TareaPendiente();
		
		tareaPendiente.setId(doc.get("_id").toString());
		tareaPendiente.setTitulo(doc.get("titulo").toString());
		tareaPendiente.setOrganizador(doc.get("organizador").toString());
		tareaPendiente.setIdTarea(doc.get("idTarea").toString());
		tareaPendiente.setFechaLimite(doc.get("fechaLimite").toString());
		tareaPendiente.setTipo(doc.get("tipo").toString());
		tareaPendiente.setCorreoAlumno(doc.get("correoAlumno").toString());

		return tareaPendiente;
	}

	public TareaPendiente crearTareaPendiente(TareaPendiente tarea) {
		Document doc = new Document();
		doc.append("titulo", tarea.getTitulo());
		doc.append("organizador", tarea.getOrganizador());
		doc.append("idTarea", tarea.getIdTarea());
		doc.append("fechaLimite", tarea.getFechaLimite());
		doc.append("tipo", tarea.getTipo());
		doc.append("correoAlumno", tarea.getCorreoAlumno());

		tareasPendientes.insertOne(doc);

		return tareaPendiente(doc);
	}

	public boolean borrarTareaPendienteEstudiante(String idTarea, String correoAlumno) {
		return tareasPendientes.deleteOne(
				Filters.and(Filters.eq("idTarea", idTarea), Filters.eq("correoAlumno", correoAlumno))
				).wasAcknowledged();
	}
	
	public void borrarTodasTareasId(String idTarea) {
		this.tareasPendientes.deleteMany(Filters.eq("idTarea", idTarea));
	}
	
	public List<TareaPendiente> getAllTareasPendientesEstudiante(String correoAlumno){
		List<TareaPendiente> listaResultado = new ArrayList<>();
		
		for (Document doc : this.tareasPendientes.find()) {
			if (doc.getString("correoAlumno").equals(correoAlumno))
				listaResultado.add(tareaPendiente(doc));
		}
		
		return listaResultado;
	}

	public void crearTareasParaTodos(TareaPendiente tareaPendiente, List<String> estudiantes) {
		if(estudiantes.size() > 0) {
			
			// A�adimos una tarea pendiente para cada alumno.
			for (String correoAlumno : estudiantes) {
				tareaPendiente.setCorreoAlumno(correoAlumno);
				crearTareaPendiente(tareaPendiente);
			}
		}
	}
}
