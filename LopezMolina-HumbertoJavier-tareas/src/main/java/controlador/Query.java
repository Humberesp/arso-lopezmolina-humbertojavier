package controlador;

import java.util.List;

import com.coxautodev.graphql.tools.GraphQLRootResolver;

import modelo.TareaPendiente;

public class Query implements GraphQLRootResolver {
    
    private final RepositorioTareasPendientes repositorioTareasPendientes;

    public Query(RepositorioTareasPendientes repositorioTareasPendientes) {
        this.repositorioTareasPendientes = repositorioTareasPendientes;
    }

    public List <TareaPendiente> getAllTareasPendientesEstudiante(String correoAlumno) {
        return repositorioTareasPendientes.getAllTareasPendientesEstudiante(correoAlumno);
    }
 
}