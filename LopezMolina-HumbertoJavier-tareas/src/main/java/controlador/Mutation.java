package controlador;

import com.coxautodev.graphql.tools.GraphQLRootResolver;

import modelo.TareaPendiente;

public class Mutation implements GraphQLRootResolver {
    
    private final RepositorioTareasPendientes repositorioTareasPendientes;

    public Mutation(RepositorioTareasPendientes repositorioTareasPendientes) {
        this.repositorioTareasPendientes = repositorioTareasPendientes;
    }
    
    public TareaPendiente createTareaPendiente(String titulo, String organizador, String idTarea,
    		String fechaLimite, String tipo, String correoAlumno) {
           	
    	TareaPendiente tareaPendiente = new TareaPendiente();
    	tareaPendiente.setTitulo(titulo);
    	tareaPendiente.setOrganizador(organizador);
    	tareaPendiente.setIdTarea(idTarea);
    	tareaPendiente.setFechaLimite(fechaLimite);
    	tareaPendiente.setTipo(tipo);
    	tareaPendiente.setCorreoAlumno(correoAlumno);
    	
    	repositorioTareasPendientes.crearTareaPendiente(tareaPendiente);
        
        return tareaPendiente;
    }
    
}