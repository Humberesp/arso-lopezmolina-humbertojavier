package servlets;

import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.servlet.annotation.WebServlet;

import com.coxautodev.graphql.tools.SchemaParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import controlador.Mutation;
import controlador.Query;
import controlador.RepositorioTareasPendientes;
import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;
import modelo.TareaPendiente;

/**
 * Clase encargada de manejas los mensajes que se almacenan
 * en la cosa del servicio de RabbitMQ.
 * 
 * Dependiendo del tipo de acci�n generar� una TareaPendiente o
 * la eliminar� para uno o para todos los usuarios ESTUDIANTES
 * existentes en el servicio usuarios.
 * 
 * TareaParaTodos -> Genera una tarea para todos los estudiantes.
 * TareaParaUno -> Genera una tarea para un estudiante en concreto.
 * BorrarParaTodos -> Elimina una tarea pendiente en todos los estudiantes.
 * TareaCompletada -> Elimina una tarea pendiente en un estudiante concreto.
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

	private static RepositorioTareasPendientes repositorioTareasPendientes;
	private static MongoClient client;
	private static Client webClient = Client.create();
	private static ConnectionFactory factory;
	private static Channel channel;
	private static Connection connection;

	private static void initDB()
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		MongoClientURI uri = new MongoClientURI(
				"mongodb://arso:arso-20@cluster0-shard-00-00-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-01-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-02-dg2jn.azure.mongodb.net:27017/tareas?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
		client = new MongoClient(uri);

		MongoDatabase mongo = client.getDatabase("tareas");
		repositorioTareasPendientes = new RepositorioTareasPendientes(mongo.getCollection("tareas"));

		initRabbitMQ();
	}

	@Override
	public void destroy() {
		super.destroy();
		client.close();
	}

	public GraphQLEndpoint()
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		super(buildSchema());
	}

	private static GraphQLSchema buildSchema()
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		initDB();

		return SchemaParser.newParser().file("schema.graphqls")
				.resolvers(new Query(repositorioTareasPendientes), new Mutation(repositorioTareasPendientes)).build()
				.makeExecutableSchema();
	}

	private static void initRabbitMQ()
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		factory = new ConnectionFactory();
		factory.setUri("amqp://kbklbfqy:VHQTVJ9UYWUojjSg1kNsLQ0WLUVYu_YH@squid.rmq.cloudamqp.com/kbklbfqy");

		connection = factory.newConnection();

		channel = connection.createChannel();

		final String exchangeName = "arso-exchange";
		final String queueName = "arso-queue";
		final String routingKey = "arso-queue";

		try {
			boolean durable = true;
			channel.exchangeDeclare(exchangeName, "direct", durable);

			boolean exclusive = false;
			boolean autodelete = false;
			Map<String, Object> properties = null;
			channel.queueDeclare(queueName, durable, exclusive, autodelete, properties);

			channel.queueBind(queueName, exchangeName, routingKey);
		} catch (IOException e) {

			String mensaje = e.getMessage() == null ? e.getCause().getMessage() : e.getMessage();

			System.out.println("No se ha podido establecer la conexion con el exchange o la cola: \n\t->" + mensaje);
			return;
		}

		boolean autoAck = false;
		channel.basicConsume(queueName, autoAck, "arso-consumidor", new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {

				String routingKey = envelope.getRoutingKey();
				String contentType = properties.getContentType();
				long deliveryTag = envelope.getDeliveryTag();

				String contenido = new String(body, "UTF-8");

				System.out.println("------MENSAJE------------: ");
				System.out.println("routingKey: " + routingKey);
				System.out.println(contenido);
				ObjectMapper mapper = new ObjectMapper();

				JsonNode json = mapper.readTree(new StringReader(contenido));
				try {
					String titulo = json.get("Titulo").asText();
					String organizador = json.get("Organizador").asText();
					String idTarea = json.get("IdTarea").asText();
					String fechaLimite = json.get("FechaLimite").asText();
					String tipo = json.get("Tipo").asText();
					String accion = json.get("Accion").asText();
					String correoAlumno = json.get("CorreoAlumno").asText();

					TareaPendiente tareaPendiente = new TareaPendiente();
					
					// Manejador de acciones
					switch (accion) {
					case "TareaParaTodos":
						tareaPendiente = new TareaPendiente();
						tareaPendiente.setTitulo(titulo);
						tareaPendiente.setOrganizador(organizador);
						tareaPendiente.setIdTarea(idTarea);
						tareaPendiente.setFechaLimite(fechaLimite);
						tareaPendiente.setTipo(tipo);

						repositorioTareasPendientes.crearTareasParaTodos(tareaPendiente, getAllEstudiantes());

						System.out.println("Creada tarea para todos los alumnos");
						break;

					case "TareaParaUno":
						tareaPendiente = new TareaPendiente();
						tareaPendiente.setTitulo(titulo);
						tareaPendiente.setOrganizador(organizador);
						tareaPendiente.setIdTarea(idTarea);
						tareaPendiente.setFechaLimite(fechaLimite);
						tareaPendiente.setTipo(tipo);
						tareaPendiente.setCorreoAlumno(correoAlumno);
						
						System.out.println("Creada tarea para el alumno " + correoAlumno);

						repositorioTareasPendientes.crearTareaPendiente(tareaPendiente);
						break;

					case "TareaCompletada":
						System.out.println("Eliminada tarea del alumno " + correoAlumno);

						repositorioTareasPendientes.borrarTareaPendienteEstudiante(idTarea, correoAlumno);
						break;
						
					case "BorrarParaTodos":
						System.out.println("Eliminada tarea de todos los alumnos");

						repositorioTareasPendientes.borrarTodasTareasId(idTarea);
						break;
						
					default:
						break;
					}

				} catch (NullPointerException e) {
					System.err.println("No se ha podido analizar alg�n campo del evento");
				}

				// Ack
				channel.basicAck(deliveryTag, false);
			}
		});

		System.out.println("Servicio RabbitMQ en ejecuci�n...");
	}

	/**
	 * M�todo de apoyo que devuelve todos los estudiantes
	 * existentes en la colecci�n del servicio usuarios.
	 */
	private static List<String> getAllEstudiantes()
			throws JsonProcessingException, ClientHandlerException, UniformInterfaceException, IOException {
		List<String> estudiantes = new ArrayList<String>();

		WebResource recurso = webClient.resource(
				"http://localhost:8080/graphql?query=" + URLEncoder.encode("{getEstudiantes{email}}", "UTF-8"));

		ClientResponse respuesta = recurso.accept("application/json;charset=utf-8").method("GET", ClientResponse.class);

		if (respuesta.getStatusInfo().toString().equals("OK")) {
			ObjectMapper mapper = new ObjectMapper();

			JsonNode json = mapper.readTree(new StringReader(respuesta.getEntity(String.class)));

			for (JsonNode node : json.findValues("email"))
				estudiantes.add(node.asText());

			return estudiantes;
		}

		return null;
	}
}
