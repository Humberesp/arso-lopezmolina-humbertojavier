package api;

import java.net.URI;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import controlador.ControladorImp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.jaxrs.PATCH;
import javassist.tools.web.BadHttpRequest;
import modelo.Respuesta;
import modelo.Sondeo;

@SwaggerDefinition(info = @Info(title = "Sondeos API", version = "V1.0.0", description = "Servicio dedicado a la gesti�n de sondeos para profesores y alumnos", contact = @Contact(name = "Humberto Javier L�pez Molina")))
@Path("sondeos")
@Api(value = "sondeos")
public class ApiRest {
	private final ControladorImp controlador = ControladorImp.getInstance();

	@Context
	private UriInfo uri;

	@POST
	@ApiOperation(value = "Crea un sondeo", notes = "M�todo que permite al profesor crear un sondeo con todas las propiedades necesarias."
			+ "Posteriormente se apoyar� en otros m�todos para a�adir las respuestas.")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_CREATED, message = "El sondeo se ha creado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Los atributos no se han especificado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response createSondeo(
			@ApiParam(value = "Correo del profesor", required = true) @FormParam("organizador") String organizador,
			@ApiParam(value = "Pregunta del sondeo", required = true) @FormParam("pregunta") String pregunta,
			@ApiParam(value = "Fecha de apertura de inscripci�n", required = true) @FormParam("aperturaInscripcion") String aperturaInscripcion,
			@ApiParam(value = "Fecha de cierre de inscripci�n", required = true) @FormParam("cierreInscripcion") String cierreInscripcion,
			@ApiParam(value = "M�ximas respuestas posibles", required = true) @FormParam("maxRespuestas") int maxRespuestas,
			@ApiParam(value = "M�nimo respuestas posibles", required = true) @FormParam("minRespuestas") int minRespuestas)
			throws IllegalArgumentException, ParseException, SondeoExcepcion {

		String idSondeo = controlador.crearSondeo(organizador, pregunta, aperturaInscripcion, cierreInscripcion,
				maxRespuestas, minRespuestas);

		UriBuilder builder = uri.getAbsolutePathBuilder();
		builder.path(idSondeo);
		URI nueva = builder.build();

		return Response.created(nueva).build();
	}

	@GET
	@Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Devuelve todos los sondeos disponibles")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, message = "Lista de sondeos devuelta con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response getSondeos() {
		List<Sondeo> sondeos = controlador.getSondeos();
		return Response.status(Response.Status.OK).entity(sondeos).build();
	}

	@GET
	@Path("/{idSondeo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Devuelve un sondeo en espec�fico", response = Sondeo.class)
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_OK, message = "Sondeo devuelto con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Los atributos no se han especificado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo con identificador idSondeo no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response getSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo) {
		Sondeo sondeo = controlador.getSondeo(idSondeo);
		return Response.status(Response.Status.OK).entity(sondeo).build();
	}
	
	@DELETE
	@Path("/{idSondeo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Elimina un sondeo en espec�fico")
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Sondeo eliminado con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo con identificador idSondeo no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response deleteSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "Correo del profesor", required = true) @FormParam("correoProfesor") String correoProfesor) throws SondeoExcepcion {
		controlador.deleteSondeo(idSondeo, correoProfesor);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@PATCH
	@Path("/{idSondeo}/respuesta")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Crea una opci�n de respuesta en un sondeo espec�fico")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Pregunta creada con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo con identificador idSondeo no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response createPreguntaSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "Correo del profesor", required = true) @FormParam("correoProfesor") String correoProfesor,
			@ApiParam(value = "Opci�n de respuesta", required = true) @FormParam("opcionRespuesta") String opcionRespuesta) throws IllegalArgumentException, SondeoExcepcion {

		controlador.createOpcionRespuestaSondeo(idSondeo, correoProfesor, opcionRespuesta);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
	
	@GET
	@Path("/{idSondeo}/respuesta")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Devuelve todas las respuestas de un sondeo", response = Respuesta.class)
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_OK, message = "Lista de respuestas devuelta con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo con identificador idSondeo no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response getRespuestasSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo) {
		List<Respuesta> respuestas = controlador.getRespuestasSondeo(idSondeo);
		return Response.status(Response.Status.OK).entity(respuestas).build();
	}
	
	@GET
	@Path("/{idSondeo}/respuesta/{idRespuesta}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Devuelve una respuesta espec�fica de un sondeo", response = Respuesta.class)
	@ApiResponses(value = { @ApiResponse(code = HttpServletResponse.SC_OK, message = "Respuesta con identificador idRespuesta"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo o respuesta no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response getRespuestaSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "identificador de la respuesta", required = true) @PathParam("idRespuesta") String idRespuesta) {
		Respuesta respuesta = controlador.getRespuestaSondeo(idSondeo, idRespuesta);
		return Response.status(Response.Status.OK).entity(respuesta).build();
	}
	
	@PATCH
	@Path("/{idSondeo}/respuesta/{idRespuesta}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Un alumno vota una opci�n de respuesta concreta")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Respuesta creada con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Los atributos no se han especificado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo o respuesta no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response createRespuestaSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "Correo del alumno", required = true) @FormParam("correoAlumno") String correoAlumno,
			@ApiParam(value = "Opci�n de respuesta", required = true) @PathParam("idRespuesta") String idRespuesta) throws IllegalArgumentException, SondeoExcepcion {

		controlador.createRespuestaSondeo(idSondeo, correoAlumno, idRespuesta);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
	
	@PUT
	@Path("/{idSondeo}/respuesta/{idRespuesta}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Un alumno elimina una votaci�n de una opci�n de respuesta concreta")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Respuesta borrada con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Los atributos no se han especificado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo o respuesta no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response deleteRespuestaAlumnoSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "Correo del alumno", required = true) @FormParam("correoAlumno") String correoAlumno,
			@ApiParam(value = "Opci�n de respuesta", required = true) @PathParam("idRespuesta") String idRespuesta) throws IllegalArgumentException, SondeoExcepcion {

		controlador.deleteRespuestaAlumnoSondeo(idSondeo, correoAlumno, idRespuesta);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
	
	@DELETE
	@Path("/{idSondeo}/respuesta/{idRespuesta}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Elimina una opci�n de respuesta si el sondeo todav�a no est� abierto")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Respuesta eliminada con �xito"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Los atributos no se han especificado correctamente"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "El sondeo o respuesta no existe"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno en el servidor") })
	public Response deleteRespuestaSondeo(
			@ApiParam(value = "identificador del sondeo", required = true) @PathParam("idSondeo") String idSondeo,
			@ApiParam(value = "Correo del alumno", required = true) @FormParam("correoProfesor") String correoProfesor,
			@ApiParam(value = "Opci�n de respuesta", required = true) @PathParam("idRespuesta") String idRespuesta) throws IllegalArgumentException, SondeoExcepcion {

		controlador.deleteRespuestaSondeo(idSondeo, correoProfesor, idRespuesta);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}
