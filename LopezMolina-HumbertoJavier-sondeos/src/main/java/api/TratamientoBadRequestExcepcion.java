package api;

import java.text.ParseException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TratamientoBadRequestExcepcion implements ExceptionMapper<ParseException> {

	@Override
	public Response toResponse(ParseException exception) {
		return Response.status(Response.Status.BAD_REQUEST)
	            .entity(exception.getMessage()).build();
	}

}