package api;

import javax.xml.ws.WebFault;

@SuppressWarnings("serial")
@WebFault
public class SondeoExcepcion extends Exception {
	protected String msg;

	public SondeoExcepcion(String msg, Throwable causa) {		
		super(msg, causa);
		this.msg = msg;
	}
	
	public SondeoExcepcion(String msg) {
		super(msg);		
		this.msg = msg;
	}
	
	public String getFaultInfo() {
		return this.msg;
	}
}