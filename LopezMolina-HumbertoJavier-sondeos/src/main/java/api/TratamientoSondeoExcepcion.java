package api;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TratamientoSondeoExcepcion implements ExceptionMapper<SondeoExcepcion> {
	@Override
	public Response toResponse(SondeoExcepcion exception) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
	            .entity(exception.getMessage()).build();
	}
}