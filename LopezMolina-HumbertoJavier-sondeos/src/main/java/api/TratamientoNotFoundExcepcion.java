package api;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;



@Provider
public class TratamientoNotFoundExcepcion implements ExceptionMapper<NotFoundExcepcion> {
	@Override
	public Response toResponse(NotFoundExcepcion exception) {
		return Response.status(Response.Status.NOT_FOUND)
	            .entity(exception.getMessage()).build();
	}
}
