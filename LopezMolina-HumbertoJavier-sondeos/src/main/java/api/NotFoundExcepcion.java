package api;

import javax.xml.ws.WebFault;

@SuppressWarnings("serial")
@WebFault
public class NotFoundExcepcion extends RuntimeException{
	protected String msg;

	public NotFoundExcepcion(String msg) {		
		super(msg);
		this.msg = msg;
	}
	
	public String getFaultInfo() {
		return this.msg;
	}
}
