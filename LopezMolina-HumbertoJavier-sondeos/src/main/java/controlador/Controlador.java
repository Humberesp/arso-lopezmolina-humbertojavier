package controlador;

import java.util.List;

import api.SondeoExcepcion;
import modelo.Respuesta;
import modelo.Sondeo;

public interface Controlador {
	
	String crearSondeo(String organizador, String pregunta, String aperturaInscripcion, String cierreInscripcion,
						int maxRespuestas, int minRespuestas) throws SondeoExcepcion, IllegalArgumentException;
	
	Sondeo getSondeo(String idSondeo) throws IllegalArgumentException;

	List<Sondeo> getSondeos();
	
	void createOpcionRespuestaSondeo(String idSondeo, String correoProfesor, String opcionRespuesta) throws IllegalArgumentException, SondeoExcepcion;
	
	void createRespuestaSondeo(String idSondeo, String correoAlumno, String idRespuesta) throws IllegalArgumentException, SondeoExcepcion;

	List<Respuesta> getRespuestasSondeo(String idSondeo);

	Respuesta getRespuestaSondeo(String idSondeo, String idRespuesta);

	void deleteRespuestaSondeo(String idSondeo, String correoProfesor, String idRespuesta) throws SondeoExcepcion;

	void deleteRespuestaAlumnoSondeo(String idSondeo, String correoAlumno, String idRespuesta) throws SondeoExcepcion;

	void deleteSondeo(String idSondeo, String correoProfesor) throws SondeoExcepcion;
}
