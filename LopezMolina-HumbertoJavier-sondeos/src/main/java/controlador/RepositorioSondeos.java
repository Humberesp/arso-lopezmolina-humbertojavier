package controlador;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

import modelo.Respuesta;
import modelo.Sondeo;

public class RepositorioSondeos {
	private MongoCollection<Document> sondeos;

	public RepositorioSondeos(MongoCollection<Document> sondeos) {
		this.sondeos = sondeos;
	}

	public Sondeo crearSondeo(Sondeo sondeo) {
		Document doc = new Document();
		doc.append("organizador", sondeo.getOrganizador());
		doc.append("pregunta", sondeo.getPregunta());
		doc.append("aperturaInscripcion", sondeo.getAperturaInscripcion());
		doc.append("cierreInscripcion", sondeo.getCierreInscripcion());
		doc.append("maxRespuestas", sondeo.getMaxRespuestas());
		doc.append("minRespuestas", sondeo.getMinRespuestas());
		doc.append("respuestas", new ArrayList<Respuesta>());

		sondeos.insertOne(doc);

		return sondeo(doc);
	}
	
	public void deleteSondeo(String idSondeo) {
		sondeos.findOneAndDelete(Filters.eq("_id", new ObjectId(idSondeo)));
	}

	@SuppressWarnings("unchecked")
	private Sondeo sondeo(Document doc) {
		Sondeo sondeo = new Sondeo();

		sondeo.setId(doc.get("_id").toString());
		sondeo.setOrganizador(doc.get("organizador").toString());
		sondeo.setPregunta(doc.get("pregunta").toString());
		sondeo.setAperturaInscripcion(doc.get("aperturaInscripcion").toString());
		sondeo.setCierreInscripcion(doc.get("cierreInscripcion").toString());
		sondeo.setMaxRespuestas(doc.getInteger("maxRespuestas"));
		sondeo.setMinRespuestas(doc.getInteger("minRespuestas"));

		ArrayList<Document> respuestasDoc = (ArrayList<Document>) doc.get("respuestas");
		ArrayList<Respuesta> respuestas = new ArrayList<Respuesta>();

		for (Document docRespuesta : respuestasDoc) {
			Respuesta respuesta = new Respuesta();

			Integer id = (Integer) docRespuesta.get("id");
			respuesta.setId(id.intValue());
			respuesta.setRespuesta((String) docRespuesta.get("respuesta"));
			respuesta.setAlumnosResponden((ArrayList<String>) docRespuesta.get("alumnosResponden"));

			respuestas.add(respuesta);
		}

		sondeo.setRespuestas(respuestas);

		return sondeo;
	}

	public Sondeo getSondeo(String idSondeo) {
		Document doc = sondeos.find(Filters.eq("_id", new ObjectId(idSondeo))).first();

		if (doc == null)
			return null;

		return sondeo(doc);
	}

	public List<Sondeo> getSondeos() {
		List<Sondeo> sondeosList = new ArrayList<Sondeo>();

		FindIterable<Document> fi = sondeos.find();
		MongoCursor<Document> cursor = fi.iterator();
		try {
			while (cursor.hasNext()) {
				sondeosList.add(sondeo(cursor.next()));
			}
		} finally {
			cursor.close();
		}

		return sondeosList;
	}

	public void createOpcionRespuestaSondeo(Respuesta nuevaRespuesta, Sondeo sondeo) {
		Document doc = new Document();

		BasicDBList dbo = new BasicDBList();

		for (Respuesta respuesta : sondeo.getRespuestas()) {
			BasicDBObject respuestaObj = new BasicDBObject();
			respuestaObj.put("id", respuesta.getId());
			respuestaObj.put("respuesta", respuesta.getRespuesta());
			respuestaObj.put("alumnosResponden", respuesta.getAlumnosResponden());
			dbo.add(respuestaObj);
		}

		BasicDBObject respuestaObj = new BasicDBObject();
		respuestaObj.put("id", nuevaRespuesta.getId());
		respuestaObj.put("respuesta", nuevaRespuesta.getRespuesta());
		respuestaObj.put("alumnosResponden", nuevaRespuesta.getAlumnosResponden());
		dbo.add(respuestaObj);

		doc.append("$set", new Document("respuestas", dbo));

		sondeos.findOneAndUpdate(Filters.eq("_id", new ObjectId(sondeo.getId())), doc);
	}

	public void actualizarSondeo(Sondeo sondeo) {
		Document doc = new Document();

		BasicDBList dbo = new BasicDBList();

		for (Respuesta respuesta_ : sondeo.getRespuestas()) {
			BasicDBObject respuestaObj = new BasicDBObject();
			respuestaObj.put("id", respuesta_.getId());
			respuestaObj.put("respuesta", respuesta_.getRespuesta());
			respuestaObj.put("alumnosResponden", respuesta_.getAlumnosResponden());
			dbo.add(respuestaObj);
		}

		doc.append("$set", new Document("respuestas", dbo));

		sondeos.findOneAndUpdate(Filters.eq("_id", new ObjectId(sondeo.getId())), doc);
	}
}
