package controlador;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import api.SondeoExcepcion;
import modelo.TareaPendiente;

/**
 * Clase que construye un comunicador del servicio RabbitMQ
 * y que se encargar� de comunicar las tareas pendientes
 * relacionadas con los sondeos.
 */
public class Productor {
	private static Productor productor;
	
	private final Connection connection;
	private final Channel channel;
	private final String exchangeName = "arso-exchange";
	private final String queueName = "arso-queue";
	private final String routingKey = "arso-queue";
	
	public static Productor getInstance() throws SondeoExcepcion {
		if(productor == null)
			productor = new Productor();
		
		return productor;
	}

	private Productor() throws SondeoExcepcion {
		ConnectionFactory factory = new ConnectionFactory();
		try {
			factory.setUri("amqp://kbklbfqy:VHQTVJ9UYWUojjSg1kNsLQ0WLUVYu_YH@squid.rmq.cloudamqp.com/kbklbfqy");
			connection = factory.newConnection();
			channel = connection.createChannel();

			boolean durable = true;
			channel.exchangeDeclare(exchangeName, "direct", durable);

			boolean exclusive = false;
			boolean autodelete = false;

			Map<String, Object> properties = null;

			channel.queueDeclare(queueName, durable, exclusive, autodelete, properties);
			channel.queueBind(queueName, exchangeName, routingKey);
		} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException | IOException
				| TimeoutException e) {
			throw new SondeoExcepcion("Error en el servicio RabbitMQ");
		}

	}

	/**
	 * Env�a una TareaPendiente a la cola de tareas del servicio
	 * de RabbitMQ con tipo "Sondeos" y una acci�n correspondiente.
	 */
	public void comunicarTarea(TareaPendiente tareaPendiente) throws SondeoExcepcion {

		String mensajeJson;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mensajeJson = mapper.writeValueAsString(tareaPendiente);

			channel.basicPublish(exchangeName, routingKey,
					new AMQP.BasicProperties.Builder().contentType("application/json").build(), mensajeJson.getBytes());

		} catch (IOException e) {
			throw new SondeoExcepcion("Error en el servicio RabbitMQ");
		}

	}
}
