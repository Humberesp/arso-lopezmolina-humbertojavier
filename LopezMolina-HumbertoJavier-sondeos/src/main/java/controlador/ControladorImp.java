package controlador;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.sun.jersey.api.NotFoundException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import api.NotFoundExcepcion;
import api.SondeoExcepcion;
import modelo.Respuesta;
import modelo.Sondeo;
import modelo.TareaPendiente;

/**
 * Controlador que implementa todos los m�todos en los que se apoya la api del
 * servicio de sondeos.
 *
 */
public class ControladorImp implements Controlador {
	private static ControladorImp controlador;
	private final MongoClient mongoClient;
	private final RepositorioSondeos repositorioSondeos;

	public static ControladorImp getInstance() {
		if (controlador == null)
			controlador = new ControladorImp();

		return controlador;
	}

	private ControladorImp() {
		MongoClientURI uri = new MongoClientURI(
				"mongodb://arso:arso-20@cluster0-shard-00-00-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-01-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-02-dg2jn.azure.mongodb.net:27017/sondeos?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");

		mongoClient = new MongoClient(uri);
		MongoDatabase mongo = mongoClient.getDatabase("sondeos");

		repositorioSondeos = new RepositorioSondeos(mongo.getCollection("sondeos"));
	}

	/**
	 * M�todo que crea un sondeo.
	 * 
	 * Recibe el correo de un profesor (Organizador), una pregunta, fechas de
	 * apertura y cierre de la inscripci�n para poder votar y el n�mero m�ximo y
	 * m�nimo de respuestas de los alumnos.
	 * 
	 * REQUISITOS 
	 * - S�lo puede crear un sondeo un profesor. 
	 * - Todos los atributos mencionados anteriormente son obligatorios, sin excepci�n. 
	 * - El n�mero m�ximo de respuestas debe ser mayor al n�mero m�nimo de respuestas.
	 * - El n�mero m�nimo de respuestas debe ser mayor que 0.
	 * - La fecha de apertura de inscripci�n debe ser menor a la de cierre y ambas mayores a la
	 * actual.
	 */
	@Override
	public String crearSondeo(String organizador, String pregunta, String aperturaInscripcion, String cierreInscripcion,
			int maxRespuestas, int minRespuestas) throws SondeoExcepcion, IllegalArgumentException {
		
		// Comprobaci�n de errores
		if (organizador == null || organizador.equals(""))
			throw new IllegalArgumentException("El atributo organizador es obligatorio");

		if (pregunta == null || pregunta.equals(""))
			throw new IllegalArgumentException("El atributo pregunta es obligatorio");

		if (aperturaInscripcion == null || aperturaInscripcion.equals(""))
			throw new IllegalArgumentException("El atributo aperturaInscripcion es obligatorio");

		if (cierreInscripcion == null || cierreInscripcion.equals(""))
			throw new IllegalArgumentException("El atributo cierreInscripcion es obligatorio");

		if (minRespuestas < 1)
			throw new IllegalArgumentException("El atributo minRespuestas es obligatorio y debe ser mayor que 0");

		if (maxRespuestas < minRespuestas)
			throw new IllegalArgumentException("El atributo maxRespuesta debe ser mayor que minRespuestas");

		Date aperturaInscripcion_ = getFecha(aperturaInscripcion);
		Date cierreInscripcion_ = getFecha(cierreInscripcion);
		Date actual = new Date();

		if (actual.compareTo(aperturaInscripcion_) > 0 || aperturaInscripcion_.compareTo(cierreInscripcion_) >= 0) {
			throw new IllegalArgumentException(
					"Las fechas deben ser posteriores a la actual y la de apertura menor a la de cierre");
		}

		String rol;
		try {
			rol = getRolUsuario(organizador);

			// Verificamos que el organizador es un profesor.
			if (rol != null && rol.equals("PROFESOR")) {
				Sondeo sondeo = new Sondeo();
				sondeo.setOrganizador(organizador);
				sondeo.setPregunta(pregunta);
				sondeo.setAperturaInscripcion(aperturaInscripcion);
				sondeo.setCierreInscripcion(cierreInscripcion);
				sondeo.setMaxRespuestas(maxRespuestas);
				sondeo.setMinRespuestas(minRespuestas);

				String sondeoId = repositorioSondeos.crearSondeo(sondeo).getId();

				// Notificar RabbitMQ para crear un sondeo pendiente a cada alumno
				TareaPendiente tareaPendiente = new TareaPendiente();

				tareaPendiente.setTitulo(sondeo.getPregunta());
				tareaPendiente.setOrganizador(sondeo.getOrganizador());
				tareaPendiente.setIdTarea(sondeoId);
				tareaPendiente.setFechaLimite(sondeo.getCierreInscripcion());
				tareaPendiente.setCorreoAlumno("");
				tareaPendiente.setTipo("Sondeo");
				tareaPendiente.setAccion("TareaParaTodos");

				Productor.getInstance().comunicarTarea(tareaPendiente);

				return sondeoId;
			} else {
				throw new IllegalArgumentException("El organizador debe corresponder a un profesor");
			}

		} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
			throw new SondeoExcepcion("Error en el servicio de usuarios");
		}
	}

	/**
	 * Elimina un sondeo.
	 * 
	 * Es importante garantizar que el que ha llamado al m�todo es un profesor.
	 * Cualquier profesor puede borrar o modificar un sondeo.
	 * 
	 * Ambos par�metros (idSondeo y correoProfesor) son obligatorios.
	 */
	@Override
	public void deleteSondeo(String idSondeo, String correoProfesor) throws SondeoExcepcion {
		// Comprobaci�n de errores.
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (correoProfesor == null || correoProfesor.equals(""))
			throw new IllegalArgumentException("El atributo correoProfesor es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		String rol;

		try {
			rol = getRolUsuario(correoProfesor);

			// Comprobamos que el correo del profesor es de verdad un profesor
			if (rol != null && rol.equals("PROFESOR")) {
				Date aperturaInscripcion = getFecha(sondeo.getAperturaInscripcion());
				Date actual = new Date();

				// Evitamos borrar el sondeo si nos encontramos en el plazo de
				// inscripci�n o �ste ya ha pasado

				if (actual.compareTo(aperturaInscripcion) >= 0) {
					throw new IllegalArgumentException(
							"El plazo de inscripci�n para borrar el sondeo debe estar cerrado.");
				} else {
					repositorioSondeos.deleteSondeo(idSondeo);

					// Notificar mediante RabbitMQ para eliminar la tarea pendiente de todos los
					// alumnos.
					TareaPendiente tareaPendiente = new TareaPendiente();

					tareaPendiente.setTitulo(sondeo.getPregunta());
					tareaPendiente.setOrganizador(sondeo.getOrganizador());
					tareaPendiente.setIdTarea(sondeo.getId());
					tareaPendiente.setFechaLimite(sondeo.getCierreInscripcion());
					tareaPendiente.setCorreoAlumno("");
					tareaPendiente.setTipo("Sondeo");
					tareaPendiente.setAccion("BorrarParaTodos");

					Productor.getInstance().comunicarTarea(tareaPendiente);

				}
			} else {
				throw new IllegalArgumentException("El atributo correoProfesor debe pertenecer a un profesor");
			}

		} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
			throw new SondeoExcepcion("Error en el servicio de usuarios");
		}
	}

	/**
	 * Obtiene un sondeo especificado por su identificador.
	 * El atributo idSondeo es obligatorio.
	 */
	@Override
	public Sondeo getSondeo(String idSondeo) throws IllegalArgumentException {
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		return sondeo;
	}

	/**
	 * Obtiene todos los sondeos existentes en la colecci�n.
	 */
	@Override
	public List<Sondeo> getSondeos() {
		List<Sondeo> sondeos = repositorioSondeos.getSondeos();

		if (sondeos == null)
			throw new NotFoundExcepcion("No existen sondeos");

		return sondeos;
	}

	/**
	 * Obtiene todas las respuestas del sondeo.
	 * Cada respuesta se corresponde a la la descripci�n de la opci�n
	 * de respuesta y a una lista que incluye a los alumnos que han
	 * respondido dicha opci�n.
	 * 
	 * El atributo idSondeo es obligatorio.
	 * 
	 * respuestas: [
	 *	  {
	 *		"id": 0
	 *	  	"respuesta": Descripci�n opci�n respuesta,
	 * 	  	"alumnosResponden": ["manuel@um.es", "humberto@um.es"]
	 *	  }
	 * ]
	 */
	@Override
	public List<Respuesta> getRespuestasSondeo(String idSondeo) {
		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("No existe el sondeo");

		return sondeo.getRespuestas();
	}

	
	/**
	 * Permite obtener una respuesta de un sondeo espec�fico.
	 * (Al igual que antes, esta respuesta incluye la descripci�n
	 * de la misma y la lista de estudiantes que la han respondido).
	 * 
	 * Los atributos idSondeo e idRespuesta son obligatorios.
	 */
	@Override
	public Respuesta getRespuestaSondeo(String idSondeo, String idRespuesta) {
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (idRespuesta == null || idRespuesta.equals(""))
			throw new IllegalArgumentException("El atributo idRespuesta es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("No existe el sondeo");

		for (Respuesta respuesta : sondeo.getRespuestas()) {
			if (Integer.toString(respuesta.getId()).equals(idRespuesta)) {
				return respuesta;
			}
		}

		throw new NotFoundException("La respuesta no existe");
	}

	/**
	 * M�todo que a�ade una opci�n de respuesta a un sondeo si esta ya no existe.
	 * Los atributos idSondeo, correoProfesor y opcionRespuesta son obligatorios.
	 * 
	 * REQUISITOS
	 * - El correoProfesor debe pertenecer a un profesor.
	 * - La fecha de apertura debe seguir cerrada para poder a�adir la opci�n.
	 */
	@Override
	public void createOpcionRespuestaSondeo(String idSondeo, String correoProfesor, String opcionRespuesta)
			throws IllegalArgumentException, SondeoExcepcion {

		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (correoProfesor == null || correoProfesor.equals(""))
			throw new IllegalArgumentException("El atributo correoProfesor es obligatorio");

		if (opcionRespuesta == null || opcionRespuesta.equals(""))
			throw new IllegalArgumentException("El atributo opcionRespuesta es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		Date aperturaInscripcion = getFecha(sondeo.getAperturaInscripcion());
		Date actual = new Date();

		// Evitamos crear opciones de respuesta si nos encontramos en el plazo de
		// inscripci�n

		if (actual.compareTo(aperturaInscripcion) >= 0) {
			throw new IllegalArgumentException(
					"El plazo de inscripci�n para crear opciones de respuesta debe estar cerrado. ");
		} else {
			try {
				String rol = getRolUsuario(correoProfesor);
				if (rol != null && rol.equals("PROFESOR")) {

					// Comprobar si ya existe la misma respuesta.
					for (Respuesta respuesta : sondeo.getRespuestas()) {
						if (respuesta.getRespuesta().equals(opcionRespuesta))
							throw new IllegalArgumentException(
									"Ya existe una opci�n de respuesta con: " + opcionRespuesta);
					}

					Respuesta respuesta = new Respuesta();
					respuesta.setRespuesta(opcionRespuesta);

					// Manejo de un peque�o id propio para las respuestas.
					int tam = sondeo.getRespuestas().size();

					if (tam == 0)
						respuesta.setId(0);
					else
						respuesta.setId(sondeo.getRespuestas().get(tam - 1).getId() + 1);

					repositorioSondeos.createOpcionRespuestaSondeo(respuesta, sondeo);
				} else {
					throw new IllegalArgumentException("El atributo correoProfesor debe pertenecer a un profesor");
				}
			} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
				throw new SondeoExcepcion("Error en el servicio de usuarios");
			}
		}
	}

	/**
	 * M�todo que permite a un estudiante "seleccionar" una respuesta de un sondeo.
	 * 
	 *  Los atributos idSondeo, correoALumno e idRespuesta son obligatorios.
	 *  
	 *  REQUISITOS
	 *  - El correo debe pertenecer a un alumno.
	 *  - La fecha de inscripci�n debe estar abierta para poder responder.
	 *  - El alumno no ha debido responder esta misma opci�n anteriormente.
	 */
	public void createRespuestaSondeo(String idSondeo, String correoAlumno, String idRespuesta)
			throws SondeoExcepcion, IllegalArgumentException {
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (correoAlumno == null || correoAlumno.equals(""))
			throw new IllegalArgumentException("El atributo correoAlumno es obligatorio");

		if (idRespuesta == null || idRespuesta.equals(""))
			throw new IllegalArgumentException("El atributo opcionRespuesta es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		Date cierreInscripcion = getFecha(sondeo.getCierreInscripcion());
		Date aperturaInscripcion = getFecha(sondeo.getAperturaInscripcion());
		Date actual = new Date();

		// Evitamos crear respuestas si no nos encontramos en el plazo de inscripci�n
		if (!(actual.compareTo(aperturaInscripcion) >= 0 && actual.compareTo(cierreInscripcion) < 0)) {
			throw new IllegalArgumentException("El plazo de inscripci�n para responder no est� abierto. ");
		} else {
			try {
				String rol = getRolUsuario(correoAlumno);
				if (rol != null && rol.equals("ESTUDIANTE")) {

					// Si a�n puede seleccionar respuestas
					int numRespondidas = getNumRespuestasAlumno(sondeo.getRespuestas(), correoAlumno);

					if (numRespondidas < sondeo.getMaxRespuestas()) {
						Respuesta respuesta = null;

						for (Respuesta respuesta_ : sondeo.getRespuestas()) {

							// A�adimos la respuesta al sondeo
							if (Integer.parseInt(idRespuesta) == respuesta_.getId()) {
								boolean existe = false;

								for (String alumnoResponde : respuesta_.getAlumnosResponden()) {
									if (alumnoResponde.equals(correoAlumno))
										existe = true;
								}

								if (!existe) {
									respuesta = respuesta_;
									respuesta.addAlumnoResponde(correoAlumno);
								} else {
									throw new IllegalArgumentException("El alumno ya ha respondido esta opci�n");
								}
							}
						}

						if (respuesta == null)
							throw new IllegalArgumentException("La respuesta no existe");

						repositorioSondeos.actualizarSondeo(sondeo);

						// Si ha respondido las m�nimas requeridas se elimina su tarea.
						if (numRespondidas + 1 >= sondeo.getMinRespuestas()) {
							// Notificar RabbitMQ para eliminar sondeo pendiente al alumno.
							TareaPendiente tareaPendiente = new TareaPendiente();

							tareaPendiente.setTitulo(sondeo.getPregunta());
							tareaPendiente.setOrganizador(sondeo.getOrganizador());
							tareaPendiente.setIdTarea(sondeo.getId());
							tareaPendiente.setFechaLimite(sondeo.getCierreInscripcion());
							tareaPendiente.setCorreoAlumno(correoAlumno);
							tareaPendiente.setTipo("Sondeo");
							tareaPendiente.setAccion("TareaCompletada");

							Productor.getInstance().comunicarTarea(tareaPendiente);

						}
					} else {
						throw new IllegalArgumentException("El alumno no puede a�adir m�s respuestas a �ste sondeo");
					}

				} else {
					throw new IllegalArgumentException("El atributo correoAlumno debe pertenecer a un estudiante");
				}
			} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
				throw new SondeoExcepcion("Error en el servicio de usuarios");
			}
		}
	}

	/**
	 * M�todo que permite a un profesor eliminar una opci�n de respuesta de un sondeo.
	 * 
	 * Los atributos idSondeo, correoProfesor e idRespuesta son obligatorios.
	 * 
	 * REQUISITOS
	 * - El correo debe pertenecer a un profesor.
	 * - La fecha de inscripci�n debe estar cerrada para poder eliminar la respuesta.
	 */
	@Override
	public void deleteRespuestaSondeo(String idSondeo, String correoProfesor, String idRespuesta)
			throws SondeoExcepcion {
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (correoProfesor == null || correoProfesor.equals(""))
			throw new IllegalArgumentException("El atributo correoProfesor es obligatorio");

		if (idRespuesta == null || idRespuesta.equals(""))
			throw new IllegalArgumentException("El atributo opcionRespuesta es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		Date aperturaInscripcion = getFecha(sondeo.getAperturaInscripcion());
		Date actual = new Date();

		// Evitamos borrar opciones de respuesta si nos encontramos en el plazo de
		// inscripci�n o �ste ya ha pasado

		if (actual.compareTo(aperturaInscripcion) >= 0) {
			throw new IllegalArgumentException(
					"El plazo de inscripci�n para borrar opciones de respuesta debe estar cerrado. ");
		} else {
			try {
				String rol = getRolUsuario(correoProfesor);
				if (rol != null && rol.equals("PROFESOR")) {

					// Buscamos y eliminamos la respuesta
					for (Respuesta respuesta : sondeo.getRespuestas()) {
						if (Integer.toString(respuesta.getId()).equals(idRespuesta)) {
							sondeo.getRespuestas().remove(respuesta);
							repositorioSondeos.actualizarSondeo(sondeo);
							return;
						}
					}

					throw new NotFoundException("No se ha encontrado la respuesta");

				} else {
					throw new IllegalArgumentException("El atributo correoProfesor debe pertenecer a un profesor");
				}
			} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
				throw new SondeoExcepcion("Error en el servicio de usuarios");
			}
		}
	}

	/**
	 * M�todo que permite a un alumno eliminar una respuesta de un sondeo.
	 * 
	 * Los atributos idSondeo, correoAlumno e idRespuesta son obligatorios.
	 * 
	 * REQUISITOS
	 * - El correo debe pertenecer a un alumno.
	 * - La fecha de inscripci�n debe estar abierta para poder eliminar la respuesta.
	 */
	@Override
	public void deleteRespuestaAlumnoSondeo(String idSondeo, String correoAlumno, String idRespuesta)
			throws SondeoExcepcion {
		if (idSondeo == null || idSondeo.equals(""))
			throw new IllegalArgumentException("El atributo idSondeo es obligatorio");

		if (correoAlumno == null || correoAlumno.equals(""))
			throw new IllegalArgumentException("El atributo correoAlumno es obligatorio");

		if (idRespuesta == null || idRespuesta.equals(""))
			throw new IllegalArgumentException("El atributo opcionRespuesta es obligatorio");

		Sondeo sondeo = repositorioSondeos.getSondeo(idSondeo);

		if (sondeo == null)
			throw new NotFoundExcepcion("El sondeo no existe");

		Date cierreInscripcion = getFecha(sondeo.getCierreInscripcion());
		Date aperturaInscripcion = getFecha(sondeo.getAperturaInscripcion());
		Date actual = new Date();

		// Evitamos crear respuestas si no nos encontramos en el plazo de inscripci�n
		if (!(actual.compareTo(aperturaInscripcion) >= 0 && actual.compareTo(cierreInscripcion) < 0)) {
			throw new IllegalArgumentException("El plazo de inscripci�n para borrar una respuesta no est� abierto. ");
		} else {
			try {
				String rol = getRolUsuario(correoAlumno);
				if (rol != null && rol.equals("ESTUDIANTE")) {
					Respuesta respuesta = null;

					for (Respuesta respuesta_ : sondeo.getRespuestas()) {
						if (Integer.parseInt(idRespuesta) == respuesta_.getId()) {
							boolean existe = false;

							for (String alumnoResponde : respuesta_.getAlumnosResponden()) {
								if (alumnoResponde.equals(correoAlumno))
									existe = true;
							}

							if (existe) {
								respuesta = respuesta_;
								respuesta.removeAlumnoResponde(correoAlumno);
							} else {
								throw new IllegalArgumentException("El alumno no respondi� a la pregunta");
							}
						}
					}

					if (respuesta == null)
						throw new IllegalArgumentException("La respuesta no existe");

					repositorioSondeos.actualizarSondeo(sondeo);

					int numRespondidas = getNumRespuestasAlumno(sondeo.getRespuestas(), correoAlumno);

					// Si al eliminar la respuesta no cumplimos el m�nimo de respondidas notificamos de nuevo.
					if (numRespondidas < sondeo.getMinRespuestas()) {
						// Notificar RabbitMQ para a�adir sondeo pendiente al alumno.
						TareaPendiente tareaPendiente = new TareaPendiente();

						tareaPendiente.setTitulo(sondeo.getPregunta());
						tareaPendiente.setOrganizador(sondeo.getOrganizador());
						tareaPendiente.setIdTarea(sondeo.getId());
						tareaPendiente.setFechaLimite(sondeo.getCierreInscripcion());
						tareaPendiente.setCorreoAlumno(correoAlumno);
						tareaPendiente.setTipo("Sondeo");
						tareaPendiente.setAccion("TareaParaUno");

						Productor.getInstance().comunicarTarea(tareaPendiente);
					}

				} else {
					throw new IllegalArgumentException("El atributo correoAlumno debe pertenecer a un estudiante");
				}
			} catch (ClientHandlerException | UniformInterfaceException | IOException e) {
				throw new SondeoExcepcion("Error en el servicio de usuarios");
			}
		}
	}

	/**
	 * M�todo de apoyo que permite obtener el rol correspondiente a un usuario.
	 * 
	 * Devuelve "ESTUDIANTE" si el correo pertenece a un usuario con el rol
	 * de estudiante, por el contrario, devuelve "PROFESOR" si el correo
	 * pertenece al usuario con rol de "PROFESOR".
	 * 
	 * Se apoya en el servicio de usuarios, que debe estar operativo.
	 */
	private static String getRolUsuario(String correoProfesor)
			throws JsonProcessingException, ClientHandlerException, UniformInterfaceException, IOException {
		Client webClient = Client.create();
		WebResource recurso = webClient.resource("http://localhost:8080/graphql?query="
				+ URLEncoder.encode("{getRolUsuario(email: \"" + correoProfesor + "\"){rol}}", "UTF-8"));

		ClientResponse respuesta = recurso.accept("application/json;charset=utf-8").method("GET", ClientResponse.class);

		if (respuesta.getStatusInfo().toString().equals("OK")) {
			ObjectMapper mapper = new ObjectMapper();

			JsonNode json = mapper.readTree(new StringReader(respuesta.getEntity(String.class)));
			return json.findValue("rol") != null ? json.findValue("rol").asText() : null;
		}

		return null;
	}

	/**
	 * M�todo de apoyo que, dada una lista de respuestas y el correo de un alumno,
	 * devuelve la cantidad de respuestas que ha realizado el alumno en el sondeo.
	 */
	private int getNumRespuestasAlumno(List<Respuesta> respuestas, String correoAlumno) {
		int num = 0;

		for (Respuesta respuesta : respuestas) {
			for (String alumnoResponde : respuesta.getAlumnosResponden()) {
				if (alumnoResponde.equals(correoAlumno)) {
					num++;
				}
			}
		}

		return num;
	}

	/**
	 * M�todo de apoyo que convierte un string, perteneciente a una fecha ISO,
	 * en un objeto Date de java.
	 */
	private Date getFecha(String fechaISO) throws SondeoExcepcion {
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			return df1.parse(fechaISO);
		} catch (ParseException e) {
			throw new SondeoExcepcion("Error al convertir la fecha, formato incorrecto");
		}
	}
}
