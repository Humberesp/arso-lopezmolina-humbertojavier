package modelo;

import java.util.ArrayList;

public class Respuesta {
	private int id;
	private String respuesta;
	private ArrayList<String> alumnosResponden;
	
	public Respuesta () {
		alumnosResponden = new ArrayList<String>();
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public ArrayList<String> getAlumnosResponden() {
		return alumnosResponden;
	}

	public void setAlumnosResponden(ArrayList<String> arrayList) {
		this.alumnosResponden = arrayList;
	}
	
	public boolean addAlumnoResponde(String correoAlumno) {
		return alumnosResponden.add(correoAlumno);
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public boolean removeAlumnoResponde(String correoAlumno) {
		return alumnosResponden.remove(correoAlumno);
		
	}
}
