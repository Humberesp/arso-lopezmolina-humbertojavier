package modelo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TareaPendiente {
	@JsonProperty("Titulo")
	private String titulo;
	
	@JsonProperty("Organizador")
	private String organizador;
	
	@JsonProperty("IdTarea")
	private String idTarea;
	
	@JsonProperty("FechaLimite")
	private String fechaLimite;
	
	@JsonProperty("CorreoAlumno")
	private String correoAlumno;
	
	@JsonProperty("Tipo")
	private String tipo;
	
	@JsonProperty("Accion")
	private String accion;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getOrganizador() {
		return organizador;
	}
	public void setOrganizador(String organizador) {
		this.organizador = organizador;
	}
	public String getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(String idTarea) {
		this.idTarea = idTarea;
	}
	public String getFechaLimite() {
		return fechaLimite;
	}
	public void setFechaLimite(String fechaLimite) {
		this.fechaLimite = fechaLimite;
	}
	public String getCorreoAlumno() {
		return correoAlumno;
	}
	public void setCorreoAlumno(String correoAlumno) {
		this.correoAlumno = correoAlumno;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	
}
