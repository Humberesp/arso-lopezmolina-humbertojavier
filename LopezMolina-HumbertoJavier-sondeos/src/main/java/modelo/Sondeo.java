package modelo;

import java.util.ArrayList;
import java.util.List;

public class Sondeo {
	private String id;
	private String organizador;
	private String pregunta;
	private String aperturaInscripcion;
	private String cierreInscripcion;
	private int maxRespuestas;
	private int minRespuestas;
	private ArrayList<Respuesta> respuestas;
	
	public Sondeo() {
		respuestas = new ArrayList<Respuesta>();
	}
	
	public boolean addRespuesta(Respuesta respuesta) {
		return respuestas.add(respuesta);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrganizador() {
		return organizador;
	}
	public void setOrganizador(String organizador) {
		this.organizador = organizador;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getAperturaInscripcion() {
		return aperturaInscripcion;
	}
	public void setAperturaInscripcion(String aperturaInscripcion) {
		this.aperturaInscripcion = aperturaInscripcion;
	}
	public String getCierreInscripcion() {
		return cierreInscripcion;
	}
	public void setCierreInscripcion(String cierreInscripcion) {
		this.cierreInscripcion = cierreInscripcion;
	}
	public int getMaxRespuestas() {
		return maxRespuestas;
	}
	public void setMaxRespuestas(int maxRespuestas) {
		this.maxRespuestas = maxRespuestas;
	}
	public int getMinRespuestas() {
		return minRespuestas;
	}
	public void setMinRespuestas(int minRespuestas) {
		this.minRespuestas = minRespuestas;
	}
	public List<Respuesta> getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(ArrayList<Respuesta> resuestas) {
		this.respuestas = resuestas;
	}
	
	
}
