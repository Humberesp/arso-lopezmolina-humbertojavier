using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Apuntate.Models
{
    public class ApuntateItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Titulo")]
        [JsonProperty("Titulo")]
        [Required]
        public string Titulo { get; set; }

        [BsonElement("Organizador")]
        [JsonProperty("Organizador")]
        [Required]
        public string Organizador { get; set; }

        [BsonElement("Descripcion")]
        [JsonProperty("Descripcion")]
        [Required]
        public string Descripcion { get; set; }

        [BsonElement("HoraInicio")]
        [JsonProperty("HoraInicio")]
        [Required]
        public DateTime  HoraInicio { get; set; }

        [BsonElement("HoraFin")]
        [JsonProperty("HoraFin")]
        [Required]
        public DateTime  HoraFin { get; set; }

        [BsonElement("NumPlazas")]
        [JsonProperty("NumPlazas")]
        [Required]
        public int NumPlazas { get; set; }

        [BsonElement("AlumnosPorGrupo")]
        [JsonProperty("AlumnosPorGrupo")]
        [Required]
        public int AlumnosPorGrupo { get; set; }

        [BsonElement("NumIntervalos")]
        [JsonProperty("NumIntervalos")]
        public int NumIntervalos { get; set; }

        [BsonElement("AperturaInscripcion")]
        [JsonProperty("AperturaInscripcion")]
        [Required]
        public DateTime AperturaInscripcion { get; set; }

        [BsonElement("CierreInscripcion")]
        [JsonProperty("CierreInscripcion")]
        [Required]
        public DateTime CierreInscripcion { get; set; }

        [BsonElement("Frecuencia")]
        [JsonProperty("Frecuencia")]
        [Required]
        public int Frecuencia { get; set; }

        [BsonElement("Turnos")]
        [JsonProperty("Turnos")]
        public List<Turno> Turnos { get; set; }
    }
}

