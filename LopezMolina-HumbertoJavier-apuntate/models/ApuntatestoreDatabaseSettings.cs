namespace Apuntate.Models
{
    public class ApuntatestoreDatabaseSettings : IApuntatestoreDatabaseSettings
    {
        public string ApuntateCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IApuntatestoreDatabaseSettings
    {
        string ApuntateCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}