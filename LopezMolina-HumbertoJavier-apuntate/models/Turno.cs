using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace Apuntate.Models
{
    public class Turno
    {
        [BsonElement("Id")]
        [JsonProperty("Id")]
        public int Id { get; set; }

        [BsonElement("PlazasDisponibles")]
        [JsonProperty("PlazasDisponibles")]
        public int PlazasDisponibles { get; set; }

        [BsonElement("AlumnosInscritos")]
        [JsonProperty("AlumnosInscritos")]
        public List<Inscripcion> AlumnosInscritos { get; set; }

        [BsonElement("AlumnosEspera")]
        [JsonProperty("AlumnosEspera")]
        public List<Inscripcion> AlumnosEspera { get; set; }

        [BsonElement("FechaInicio")]
        [JsonProperty("FechaInicio")]
        public DateTime  FechaInicio { get; set; }

        [BsonElement("FechaFin")]
        [JsonProperty("FechaFin")]
        public DateTime  FechaFin { get; set; }        
    }
}

