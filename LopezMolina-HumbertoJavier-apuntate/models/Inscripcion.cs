using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace Apuntate.Models
{
    public class Inscripcion
    {

        [BsonElement("AlumnoId")]
        [JsonProperty("AlumnoId")]
        public string AlumnoId { get; set; }

        [BsonElement("Comentario")]
        [JsonProperty("Comentario")]
        public string Comentario { get; set; }
    }
}

