using Apuntate.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace Apuntate.Services
{
    public class ApuntateService
    {
        private readonly IMongoCollection<ApuntateItem> _todosApuntate;

        public ApuntateService(IApuntatestoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _todosApuntate = database.GetCollection<ApuntateItem>(settings.ApuntateCollectionName);
        }

        // GET All Apúntate
        public List<ApuntateItem> Get() =>
            _todosApuntate.Find(apuntate => true).ToList();

        // GET ID Apúntate
        public ApuntateItem Get(string id) =>
            _todosApuntate.Find<ApuntateItem>(apuntate => apuntate.Id == id).FirstOrDefault();

        // POST Apúntate
        public ApuntateItem Create(ApuntateItem apuntate)
        {
            _todosApuntate.InsertOne(apuntate);
            return apuntate;
        }

        public void Update(string id, ApuntateItem apuntateIn) =>
            _todosApuntate.ReplaceOne(apuntate => apuntate.Id == id, apuntateIn);

        public void Remove(ApuntateItem apuntateIn) =>
            _todosApuntate.DeleteOne(apuntate => apuntate.Id == apuntateIn.Id);

        public void Remove(string id) => 
            _todosApuntate.DeleteOne(apuntate => apuntate.Id == id);
    }
}