using System;
using Apuntate.Models;
using System.Collections.Generic;

namespace Apuntate.Controllers
{
    public class ControladorErrores
    {
        public static String comprobarRequeridos(String Titulo, String Organizador, String Descripcion, 
            DateTime HoraInicio, DateTime HoraFin, DateTime AperturaInscripcion, DateTime CierreInscripcion, 
            int AlumnosPorGrupo, int NumPlazas, int Frecuencia) {

                // Atributos no introducidos y requeridos.
                if(Titulo == null)
                    return "Titulo es un atributo requerido";

                if(Organizador == null)
                    return "Organizador es un atributo requerido";
                
                if(Descripcion == null)
                    return "Descripcion es un atributo requerido";

                if(HoraInicio == null)
                    return "HoraInicio es un atributo requerido";

                if(HoraFin == null)
                    return "HoraFin es un atributo requerido";

                if(AperturaInscripcion == null || AperturaInscripcion == DateTime.Parse("01/01/0001 0:00:00"))
                    return "AperturaInscripcion es un atributo requerido";

                if(CierreInscripcion == null || CierreInscripcion == DateTime.Parse("01/01/0001 0:00:00"))
                    return "CierreInscripcion es un atributo requerido";

                if(CierreInscripcion <= AperturaInscripcion)
                    return "La fecha de cierre de inscripción debe ser mayor que la de apertura";

                // Valores inválidos
                if(AlumnosPorGrupo == 0)
                    return "AlumnosPorGrupo es un atributo requerido y no puede ser 0";

                if(NumPlazas == 0)
                    return "NumPlazas  es un atributo requerido y no puede ser 0";

                if(AlumnosPorGrupo > NumPlazas)
                    return "El número de AlumnosPorGrupo no puede ser mayor que el NumPlazas";

                if(Frecuencia <= 0)
                    return "La frecuencia de la reunión debe ser mayor o igual a 1 días";

                return "OK";
        }

        public static Boolean comprobarExisteAlumnoInscrito(List<Turno> turnos, String alumnoId) {
            foreach (var turno in turnos)
            {
                foreach (var inscripcion in turno.AlumnosInscritos)
                {
                    if(inscripcion.AlumnoId == alumnoId)
                        return true;
                }

                foreach (var inscripcion in turno.AlumnosEspera)
                {
                    if(inscripcion.AlumnoId == alumnoId)
                        return true;
                }
            }

            return false;
        }

        public static Boolean existeTurno(List<Turno> turnos, int idTurno) {
            foreach (var turno in turnos)
            {
                if(turno.Id == idTurno)
                    return true;
            }

            return false;
        }
    }
}