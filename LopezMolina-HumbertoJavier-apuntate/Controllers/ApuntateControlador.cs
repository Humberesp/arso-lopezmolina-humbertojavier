using Apuntate.Models;
using Apuntate.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Apuntate.RabbitNQ;

namespace Apuntate.Controllers
{
    [Produces("application/json")]
    [Route("api/apuntate")]
    [ApiController]
    public class ApuntateControlador : ControllerBase
    {
        private readonly ApuntateService _apuntateService;

        public ApuntateControlador(ApuntateService apuntateService)
        {
            _apuntateService = apuntateService;
        }

        /// <summary>
        /// Obtiene todas las reuniones almacenadas en la colección.
        /// </summary>
        /// <response code="200">Todas las reuniones o una lista vacía</response>
        [HttpGet]
        public ActionResult<List<ApuntateItem>> Get() =>
            _apuntateService.Get();

        /// <summary>
        /// Obtiene una reunión con un identificador concreto.
        /// </summary>
        /// <response code="200">Reunión concreta</response>
        /// <response code="404">La reunión no existe</response>
        [HttpGet("{idApuntate:length(24)}", Name = "GetApuntate")]
        public ActionResult<ApuntateItem> Get(string idApuntate)
        {
            var apuntate = _apuntateService.Get(idApuntate);

            if (apuntate == null)
            {
                return NotFound("No existe la reunión");
            }

            return apuntate;
        }

        /**
          * Método que maneja la petición de creación de una Reunión. Si todos los
          * parámetros introducidos son correctos generará una reunión con una lista
          * de turnos (horarios) creados automáticamente en base a parámetros como
          * el número de plazas totales / número alumnos por grupo.
          */
        /// <summary>
        /// Crea una reunión (Disponible sólo para el rol de profesor)
        /// </summary>
        /// <response code="200">Reunión creada</response>
        /// <response code="400">Error en los atributos</response>
        /// <response code="503">El servicio de usuarios no está disponible</response>
        [HttpPost]
        public async Task<ActionResult<ApuntateItem>> Create(ApuntateItem apuntate)
        {
            String mensajeError = ControladorErrores.comprobarRequeridos(apuntate.Titulo, apuntate.Organizador,
                apuntate.Descripcion, apuntate.HoraInicio, apuntate.HoraFin,
                apuntate.AperturaInscripcion, apuntate.CierreInscripcion, apuntate.AlumnosPorGrupo, apuntate.NumPlazas, apuntate.Frecuencia);

            if(mensajeError == "OK") {
                
                String rol = await getRolUsuario(apuntate.Organizador);

                if(rol != null) {
                    if(rol == "PROFESOR") {
                        // Creamos los turnos
                        crearTurnos(apuntate);

                        _apuntateService.Create(apuntate);

                        // Enviamos notificación al consumidor Tareas pendientes para crear una tarea para cada alumno.
                        TareaPendiente tareaPendiente = new TareaPendiente(apuntate, "TareaParaTodos", "");
                        Productor.Instance.notificarReunion(tareaPendiente);

                        return CreatedAtRoute("GetApuntate", new { idApuntate = apuntate.Id.ToString() }, apuntate);
                    } else {
                        return BadRequest("El organizador debe ser un profesor");
                    }
                } else {
                    return BadRequest("El usuario no ha podido ser encontrado");
                }
            } else {
                return BadRequest(mensajeError);
            }
        }


        /// <summary>
        /// Elimina una reunión por completo.
        /// </summary>
        /// <response code="204">Reunión borrada correctamente</response>
        /// <response code="404">La reunión no existe</response>
        [HttpDelete("{idApuntate:length(24)}")]
        public IActionResult DelApuntate(string idApuntate)
        {
            var apuntate = _apuntateService.Get(idApuntate);

            if (apuntate == null)
            {
                return NotFound("No existe la reunión");
            }

            _apuntateService.Remove(apuntate.Id);

            // Enviamos notificación al consumidor Tareas pendientes para borrar la tarea pendiente de todos los alumnos.
            TareaPendiente tareaPendiente = new TareaPendiente(apuntate, "BorrarParaTodos", "");
            Productor.Instance.notificarReunion(tareaPendiente);

            return NoContent();      
        }

        /// <summary>
        /// Obtiene todos los turnos de una reunión concreta.
        /// </summary>
        /// <response code="200">Turnos de la reunión concreta</response>
        /// <response code="404">La reunión no existe</response>
        [HttpGet("{idApuntate:length(24)}/turno", Name = "GetAllTurno")]
        public ActionResult<List<Turno>> GetAllTurno(string idApuntate)
        {
            var apuntate = _apuntateService.Get(idApuntate);

            if (apuntate == null)
            {
                return NotFound("No existe la reunión");
            }

            return apuntate.Turnos;
        }

        /// <summary>
        /// Obtiene un turno concreto de una reunión concreta.
        /// </summary>
        /// <response code="200">Turno concreto</response>
        /// <response code="404">El turno no existe</response>
        [HttpGet("{idApuntate:length(24)}/turno/{idTurno}", Name = "GetTurno")]
        public ActionResult<Turno> Get(string idApuntate, int idTurno)
        {
            var apuntate = _apuntateService.Get(idApuntate);

            if (apuntate == null)
            {
                return NotFound("No existe la reunión");
            }

            foreach (var turno in apuntate.Turnos)
            {
                if(turno.Id == idTurno)
                    return turno;
            }

            return NotFound("No existe el turno");
        }

        /**
          * Añade un estudiante a un turno concreto sólo si el horario de inscripción
          * está abierto y si dicho usuario tiene el rol de estudiante.
          *
          * El estudiante será asignado a la lista de inscripciones si existen plazas
          * disponibles para dicho turno, de lo contrario será almacenado en la lista
          * de espera.
          */
        /// <summary>
        /// Crea una inscripción en un turno y una reunión en concreto.
        /// </summary>
        /// <response code="204">Inscripción creada correctamente</response>
        /// <response code="400">El atributo alumnoId no se ha especificado o no corresponde a un ESTUDIANTE o la fecha de inscripción está cerrada</response>
        /// <response code="404">La reunión no existe</response>
        [HttpPut("{idApuntate:length(24)}/turno/{idTurno}")]
        public async Task<IActionResult> UpdateAddInscripcion(string idApuntate, int idTurno, Inscripcion inscripcion)
        {
            if(inscripcion.AlumnoId != null) {
                var apuntate = _apuntateService.Get(idApuntate);

                if (apuntate == null)
                {
                    return NotFound("No existe la reunión");
                }

                if(!ControladorErrores.existeTurno(apuntate.Turnos, idTurno)) {
                    return NotFound("No existe el turno");
                }

                // Comprobamos que el usuario es un ESTUDIANTE.
                String rol = await getRolUsuario(inscripcion.AlumnoId);

                if(rol == "ESTUDIANTE") {
                    // Añadimos el usuario a donde corresponda si las inscripciones están abiertas.
                    if(isInscripcionAbierta(apuntate.AperturaInscripcion, apuntate.CierreInscripcion)) {
                        Boolean resultado = AddUsuarioATurno(apuntate, idTurno, inscripcion);

                        if(resultado) {
                            _apuntateService.Update(idApuntate, apuntate);

                            // Enviamos notificación al consumidor Tareas pendientes para eliminar la tarea pendiente del alumno.
                            TareaPendiente tareaPendiente = new TareaPendiente(apuntate, "TareaCompletada", inscripcion.AlumnoId);
                            Productor.Instance.notificarReunion(tareaPendiente);

                            return NoContent();
                        } else {
                            return BadRequest("El estudiante ya se encuentra inscrito o en la lista de espera de un turno de esta reunión");
                        }
                    } else {
                        return BadRequest("No se puede crer la inscripción porque la fecha de inscripción está cerrada");
                    }
                } else {
                    return BadRequest("El atributo alumnoId debe ser un email correspondiente a un ESTUDIANTE");
                }
            } else {
                return BadRequest("El atributo alumnoId es obligatorio");
            }
        }

        /**
          * Método que elimina un usuario de un turno y reunión concretas.
          * 
          * Si el estudiante que ha sido eliminado se encontraba en la lista de inscripción,
          * éste será eliminado y, en caso de que la lista de espera no esté vacía, el
          * primer estudiante que se encuentre en dicha lista será movido a la lista
          * de inscripción. Si la lista de espera está vacía se incrementan las plazas
          * disponibles al eliminar dicho usuario.
          *
          * En caso de que el estudiante se encuentre en la lista de espera, éste simplemente
          * es removido de dicha lista, sin ninguna acción especial.
          */
        /// <summary>
        /// Elimina una inscripción de un turno y una reunión en concreto.
        /// </summary>
        /// <response code="204">Inscripción borrada correctamente</response>
        /// <response code="400">El atributo alumnoId no se ha especificado o no se encuentra en el horario.</response>
        /// <response code="404">La reunión no existe</response>
        [HttpDelete("{idApuntate:length(24)}/turno/{idTurno}")]
        public IActionResult UpdateDelInscripcion(string idApuntate, int idTurno, String alumnoId)
        {
            if(alumnoId != null) {
                var apuntate = _apuntateService.Get(idApuntate);

                if (apuntate == null)
                {
                    return NotFound("No existe la reunión");
                }

                if(!ControladorErrores.existeTurno(apuntate.Turnos, idTurno)) {
                    return NotFound("No existe el turno");
                }

                // Consideramos que un usuario puede eliminar su inscripción
                // en cualquier momento aunque haya finalizado el plazo de inscripción.
                Boolean resultado = DelUsuarioTurno(apuntate, idTurno, alumnoId);
                if(resultado) {
                    _apuntateService.Update(idApuntate, apuntate);

                    // Enviamos notificación al consumidor Tareas pendientes para volver a crear la tarea pendiente del alumno.
                    TareaPendiente tareaPendiente = new TareaPendiente(apuntate, "TareaParaUno", alumnoId);
                    Productor.Instance.notificarReunion(tareaPendiente);

                     return NoContent();
                } else {
                    return BadRequest("El alumno no se encuentra en el turno especificado");
                }
            } else {
                return BadRequest("El atributo alumnoId es obligatorio");
            }
            
        }

        /**
          * Función de apoyo para generar los turnos.
          *
          * Se calcula el número de intervalos y el tiempo
          * de inicio y fin de cada uno de ellos, inicializando
          * las listas de inscripción y espera.
          */
        private void crearTurnos(ApuntateItem apuntate) {
            // Cálculo par obtener los minutos totales que transcurren entre las dos fechas.
            int horaInicio = apuntate.HoraInicio.Hour;
            int minInicio = apuntate.HoraInicio.Minute;

            int horaFin = apuntate.HoraFin.Hour;
            int minFin = apuntate.HoraFin.Minute;

            int horas, minutos;

            horas = horaFin - horaInicio;
            minutos = minFin - minInicio;

            minutos += horas * 60;

            // Calculamos el número de intervalos automáticamente.
            apuntate.NumIntervalos = apuntate.NumPlazas / apuntate.AlumnosPorGrupo;

            if(apuntate.NumPlazas % apuntate.AlumnosPorGrupo > 0)
                apuntate.NumIntervalos++;

            // Creamos tantos turnos como intervalos existan e inicializamos
            // todos los valores necesarios.
            DateTime fechaActual = apuntate.HoraInicio;
            DateTime fechaInicio = apuntate.HoraInicio;

            int minutosIntervalo = minutos / apuntate.NumIntervalos;
            
            List<Turno> turnos = new List<Turno>();

            for(int j = 0; j < apuntate.Frecuencia; j++) {
                for(int i = 0; i < apuntate.NumIntervalos; i++) {
                Turno turno = new Turno();
                turno.Id = i;
                turno.AlumnosEspera = new List<Inscripcion>();
                turno.AlumnosInscritos = new List<Inscripcion>();
                turno.PlazasDisponibles = apuntate.AlumnosPorGrupo;

                turno.FechaInicio = fechaActual;
                fechaActual = fechaActual.AddMinutes(minutosIntervalo);
                turno.FechaFin = fechaActual;

                turnos.Add(turno);
              }

              // Nuevos turnos (frecuencia) al dia siguiente.
              fechaInicio = fechaInicio.AddDays(1);
              fechaActual = fechaInicio;
            }

            apuntate.Turnos = turnos;
        }

        /**
          * Función de apoyo para añadir un usuario a un turno y reunión concretos.
          * 
          * El estudiante será asignado a la lista de inscripciones si existen plazas
          * disponibles para dicho turno, de lo contrario será almacenado en la lista
          * de espera. (Ambas acciones se llevan a cabo si el usuario ya no realizó
          * una inscripción en dicho turno anteriormente)
          */
        private Boolean AddUsuarioATurno(ApuntateItem apuntate, int idTurno, Inscripcion inscripcion) {
            if(!ControladorErrores.comprobarExisteAlumnoInscrito(apuntate.Turnos, inscripcion.AlumnoId)) {
                foreach (var turno in apuntate.Turnos)
                {
                    if(turno.Id == idTurno) {
                        if(turno.PlazasDisponibles > 0) {
                            Boolean existe = false;

                            // Insertamos el usuario en la lista de inscripciones si no está ya incluido
                            foreach (var inscripcion_ in turno.AlumnosInscritos)
                            {
                                if(inscripcion_.AlumnoId == inscripcion.AlumnoId) {
                                    existe = true;
                                }
                            }
                            foreach (var inscripcion_ in turno.AlumnosEspera)
                            {
                                if(inscripcion_.AlumnoId == inscripcion.AlumnoId) {
                                    existe = true;
                                }
                            }

                            if(!existe) {
                                turno.AlumnosInscritos.Add(inscripcion);
                                turno.PlazasDisponibles--;
                                return true;
                            }


                        } else {
                            Boolean existe = false;

                            // Insertamos el usuario en la lista de espera si no está ya incluido
                            foreach (var inscripcion_ in turno.AlumnosEspera)
                            {
                                if(inscripcion_.AlumnoId == inscripcion.AlumnoId) {
                                    existe = true;
                                }
                            }
                            foreach (var inscripcion_ in turno.AlumnosInscritos)
                            {
                                if(inscripcion_.AlumnoId == inscripcion.AlumnoId) {
                                    existe = true;
                                }
                            }

                            if(!existe) {
                                turno.AlumnosEspera.Add(inscripcion);
                                return true;
                            }
                        }

                        return false;
                    }
                }
            } else {
                return false;
            }

            return false;
        }

        /**
          * Función de apoyo para eliminar a un estudiante de un turno y reunión concretos.
          *
          * Si el estudiante que ha sido eliminado se encontraba en la lista de inscripción,
          * éste será eliminado y, en caso de que la lista de espera no esté vacía, el
          * primer estudiante que se encuentre en dicha lista será movido a la lista
          * de inscripción. Si la lista de espera está vacía se incrementan las plazas
          * disponibles al eliminar dicho usuario.
          *
          * En caso de que el estudiante se encuentre en la lista de espera, éste simplemente
          * es removido de dicha lista, sin ninguna acción especial.
          */
        private Boolean DelUsuarioTurno(ApuntateItem apuntate, int idTurno, String alumnoId) {
            foreach (var turno in apuntate.Turnos)
            {
                if(turno.Id == idTurno) {
                    // Si el usuario está en la lista de inscritos, se elimina y se pasa el primero
                    // de la lista de espera a la lista de inscritos en caso de que existan alguno.
                    foreach (var inscripcion in turno.AlumnosInscritos)
                    {
                        if(inscripcion.AlumnoId == alumnoId) {
                            turno.AlumnosInscritos.Remove(inscripcion);

                            if(turno.AlumnosEspera.Count > 0) {
                                turno.AlumnosInscritos.Add(turno.AlumnosEspera[0]);
                                turno.AlumnosEspera.Remove(turno.AlumnosEspera[0]);
                            } else {
                                turno.PlazasDisponibles++;
                            }

                            return true;
                        }
                    }
                    
                    // Si por el contrario el usuario se encuentra en la lista de espera
                    // simplemente lo eliminamos.
                    foreach (var inscripcion in turno.AlumnosEspera)
                    {
                        if(inscripcion.AlumnoId == alumnoId) {
                            turno.AlumnosEspera.Remove(inscripcion);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /**
         * Comprueba si la fecha de inscripción está activa.
         */
        private Boolean isInscripcionAbierta(DateTime aperturaInscripcion, DateTime cierreInscripcion) {
            // Si la fecha de Inicio de inscripción es menor que la actual
            // y la fecha de cierre de inscripción es mayor que la actual
            // estamos dentro de la fecha de inscripción, en caso contrario
            // o se nos ha pasado o todavía no ha empezado la inscripción.
            DateTime fechaActual = DateTime.Now;

            if(DateTime.Compare(aperturaInscripcion, fechaActual) <= 0
                && DateTime.Compare(fechaActual, cierreInscripcion) <= 0) {
                return true;
            } else {
                return false;
            }
        }

        /** Llamada a la API de usuarios para obtener el rol de un
          * usuario dado su identificador (email).
          */
        private async Task<String> getRolUsuario(String email) {
            HttpClient client = new HttpClient();

            try 
            {
                HttpResponseMessage response = await client.GetAsync("http://localhost:8080/graphql?query={getRolUsuario(email:\"" + email + "\"){rol}}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                dynamic json = JsonConvert.DeserializeObject(responseBody);

                if(json.data.getRolUsuario != null)
                    return json.data.getRolUsuario.rol;
                else
                    return null;
            }  
            catch(HttpRequestException e)
            {
                Console.WriteLine("Excepción al buscar el rol del usuario {0}", email);    
                Console.WriteLine("Error :{0} ",e.Message);
            }

            client.Dispose();
            return null;
        }
    }
}