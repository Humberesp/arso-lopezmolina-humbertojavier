using System;
using RabbitMQ.Client;
using System.Text;
using Apuntate.RabbitNQ;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Apuntate.Controllers
{
    public class Productor
    {
        private ConnectionFactory factory;
        private IConnection connection;
        private IModel channel;
        private IBasicProperties props;

        private readonly static Productor productor = new Productor();

        public static Productor Instance {
            get
            {
                return productor;
            }
        }

        private Productor() {
            // Código del productor.
            factory = new ConnectionFactory() { Uri = new Uri("amqp://kbklbfqy:VHQTVJ9UYWUojjSg1kNsLQ0WLUVYu_YH@squid.rmq.cloudamqp.com/kbklbfqy") };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.ExchangeDeclare(exchange: "arso-exchange", 
                                type: "direct", 
                                durable: true, 
                                autoDelete: false, 
                                arguments: null);

            channel.QueueDeclare(queue: "arso-queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.QueueBind(queue: "arso-queue", 
                                exchange: "arso-exchange", 
                                routingKey: "arso-queue");


            props = channel.CreateBasicProperties();
            props.ContentType = "application/json";
        }

        public Boolean notificarReunion(TareaPendiente tareaPendiente) {
            Console.WriteLine(tareaPendiente.Titulo);
            string json = JsonSerializer.Serialize(tareaPendiente, new JsonSerializerOptions{WriteIndented = true});

            var body = Encoding.UTF8.GetBytes(json);
            
            channel.BasicPublish(exchange: "arso-exchange",
                                 routingKey: "arso-queue",
                                 basicProperties: props,
                                 body: body);

            Console.WriteLine(" [x] Enviado {0}", json);

            return true;
        }
    }
}