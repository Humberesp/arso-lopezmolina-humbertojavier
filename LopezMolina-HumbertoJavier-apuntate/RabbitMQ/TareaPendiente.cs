using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using Apuntate.Models;

namespace Apuntate.RabbitNQ
{
    public class TareaPendiente
    {
        [BsonElement("Titulo")]
        [JsonProperty("Titulo")]
        public string Titulo { get; set; }

        [BsonElement("Organizador")]
        [JsonProperty("Organizador")]
        public string Organizador { get; set; }

        [BsonElement("IdTarea")]
        [JsonProperty("IdTarea")]
        public string IdTarea { get; set; }

        [BsonElement("FechaLimite")]
        [JsonProperty("FechaLimite")]
        public DateTime FechaLimite { get; set; }

        [BsonElement("CorreoAlumno")]
        [JsonProperty("CorreoAlumno")]
        public string CorreoAlumno { get; set; }

        [BsonElement("Tipo")]
        [JsonProperty("Tipo")]
        public string Tipo { get; set; }

        [BsonElement("Accion")]
        [JsonProperty("Accion")]
        public string Accion { get; set; }

        public TareaPendiente(ApuntateItem apuntate, String accion, String correoAlumno) {
            Titulo = apuntate.Titulo;
            Organizador = apuntate.Organizador;
            IdTarea = apuntate.Id;
            FechaLimite = apuntate.CierreInscripcion;
            Tipo = "Apuntate";
            Accion = accion;
            CorreoAlumno = correoAlumno;
        }
    }
}

