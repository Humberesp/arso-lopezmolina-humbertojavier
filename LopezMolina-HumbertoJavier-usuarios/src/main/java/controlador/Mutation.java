package controlador;

import com.coxautodev.graphql.tools.GraphQLRootResolver;

import modelo.Usuario;


public class Mutation implements GraphQLRootResolver {
    
    private final RepositorioUsuarios repositorioUsuarios;

    public Mutation(RepositorioUsuarios repositorioUsuarios) {
        this.repositorioUsuarios = repositorioUsuarios;
    }
    
    public Usuario createUsuario(String nombre, String email, String rol) {
        Usuario usuario = new Usuario(nombre, email, rol);
       
        repositorioUsuarios.createUsuario(usuario);
        
        return usuario;
    }
}