package controlador;

import java.util.List;

import com.coxautodev.graphql.tools.GraphQLRootResolver;

import modelo.Rol;
import modelo.Usuario;

public class Query implements GraphQLRootResolver {
    
    private final RepositorioUsuarios repositorioUsuarios;

    public Query(RepositorioUsuarios repositorioUsuarios) {
        this.repositorioUsuarios = repositorioUsuarios;
    }

    public List <Usuario> getEstudiantes() {
        return repositorioUsuarios.getUsuariosByRol(Rol.ESTUDIANTE);
    }
    
    public List <Usuario> getProfesores() {
    	return repositorioUsuarios.getUsuariosByRol(Rol.PROFESOR);
    }
    
    public Usuario getRolUsuario(String email) {
    	return repositorioUsuarios.getRolUsuario(email);
    }
}