package controlador;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import modelo.Rol;
import modelo.Usuario;

public class RepositorioUsuarios {

	private final MongoCollection<Document> usuarios;

	public RepositorioUsuarios(MongoCollection<Document> usuarios) {
		this.usuarios = usuarios;
	}
	
	/**
	 * Obtiene una lista con todos los usuarios asociados al rol pas�do como par�metro.
	 * @param rol Rol por el que se filtran los usuarios.
	 * @return lista de usuarios filtrados por el rol.
	 */
	public List<Usuario> getUsuariosByRol(Rol rol) {
		List<Usuario> results = new ArrayList<>();
		
		for (Document doc : usuarios.find()) {
			Usuario u = usuario(doc);
			
			if (u.getRol().equals(rol.toString()))
				results.add(u);
		}

		return results;
	}
	
	/**
	 * Obtiene una lista con todos los usuarios asociados al rol pas�do como par�metro.
	 * @param rol Rol por el que se filtran los usuarios.
	 * @return lista de usuarios filtrados por el rol.
	 */
	public Usuario getRolUsuario(String email) {		
		for (Document doc : usuarios.find()) {
			Usuario u = usuario(doc);
			
			if (u.getEmail().equals(email)) {
				return u;
			}
		}
		
		return null;
	}
	
	/**
	 * Funci�n de ayuda para crear usuarios y realizar pruebas.
	 * @param usuario Usuario que ser� registrado
	 * @return Devuelve el usuario si se ha registrado
	 * correctamente y null en caso de que ya existe un
	 * usuario con el email.
	 */
	public Usuario createUsuario(Usuario usuario) {
		Document doc = new Document();
		doc.append("nombre", usuario.getNombre());
		doc.append("email", usuario.getEmail());
		doc.append("rol", usuario.getRol());
		
		if(isValidUsuario(usuario.getEmail())) {
			usuarios.insertOne(doc);
			return usuario(doc);
		} else {
			return null;
		}
	}
	
	/**
	 * Comprueba si ya existe un usuario en la base de datos
	 * con el email pasado como par�metro.
	 * @param email
	 * @return true si el usuario se puede registrar, false
	 * en caso contrario.
	 */
	private boolean isValidUsuario(String email) {
		for (Document doc : usuarios.find()) {
			Usuario u = usuario(doc);
			
			if (u.getEmail().equals(email))
				return false;
		}

		return true;
	}

	private Usuario usuario(Document doc) {
		return new Usuario(doc.get("nombre").toString(), doc.getString("email"), doc.getString("rol"));
	}
}
