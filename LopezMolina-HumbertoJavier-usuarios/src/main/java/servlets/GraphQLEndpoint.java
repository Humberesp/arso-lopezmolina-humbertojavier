package servlets;

import javax.servlet.annotation.WebServlet;

import com.coxautodev.graphql.tools.SchemaParser;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

import controlador.RepositorioUsuarios;
import controlador.Mutation;
import controlador.Query;
import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

	private static RepositorioUsuarios repositorioUsuarios;
	private static MongoClient client;

	private static void initDB() {
		MongoClientURI uri = new MongoClientURI(
				"mongodb://arso:arso-20@cluster0-shard-00-00-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-01-dg2jn.azure.mongodb.net:27017,cluster0-shard-00-02-dg2jn.azure.mongodb.net:27017/arso?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
		client = new MongoClient(uri);

		MongoDatabase mongo = client.getDatabase("arso");
		repositorioUsuarios = new RepositorioUsuarios(mongo.getCollection("usuarios"));
	}

	@Override
	public void destroy() {
		super.destroy();
		client.close();
	}

	public GraphQLEndpoint() {
		super(buildSchema());
	}

	private static GraphQLSchema buildSchema() {
		initDB();
		
		return SchemaParser.newParser().file("schema.graphqls")
				.resolvers(new Query(repositorioUsuarios), new Mutation(repositorioUsuarios)).build().makeExecutableSchema();
	}
}
